<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;

class LiensController extends Controller
{
    public function index() {
       return view('welcome');
    }
    public function short(Request $request) { // $request récupére le lien long soumit
        $token = Str::random(8); //génére le token

        //sauvegarde dans la BDD
        DB::table('liens')->insert([
            "lien_long" => $request["lien"],
            "lien_court" => URL::to("/") . '/' . $token,

        ]);
      return URL::to("/") . '/' . $token; //renvoi le lien court
   }

   public function getLien($lien) {
       $lienCourt = URL::to("/") . '/' . $lien;
    $query = DB::table('liens')->where('lien_court', "=", $lienCourt);

    if ($query->exists()) { // Si le lien court existe en BDD
        return redirect($query->value('lien_long'));
    } else {
        return "Ce lien n'existe pas";
    }
   }
}

