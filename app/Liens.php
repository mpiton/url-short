<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Liens extends Model
{
    protected $fillable = [
       "lien_long",
       "lien_court"
    ];
}
