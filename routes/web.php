<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', "LiensController@index")->name('index');
Route::post('/u', "LiensController@short")->name('short');
Route::get('/{lien}', 'LiensController@getLien')->name('getLien');
